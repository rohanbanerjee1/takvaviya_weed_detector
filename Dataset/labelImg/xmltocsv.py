import xml.etree.ElementTree as ET
import os
import pandas as pd
import csv
# file="Part11.xml"
def convert(filepath,file):
    print(filepath,file)
    tree = ET.parse(filepath+'xml/'+file)
    root = tree.getroot()

    # open a file for writing
    if not os.path.exists(filepath+'csvconverted/'):
        os.makedirs(filepath+'csvconverted/')
    # Resident_data = open(file[:-3]+'csv', 'w')
    converted_data = open(filepath+'csvconverted/'+file[:-3] + 'csv', 'w')
    # create the csv writer object

    csvwriter = csv.writer(converted_data)
    head = []
    name=root.find("filename").text
    # head.append('path')
    # head.append('xmin')
    # head.append('ymin')
    # head.append('xmax')
    # head.append('ymax')
    # head.append('name')
    # csvwriter.writerow(head)
    for member in root.findall('object'):
        values=[]
        # name=member.find('name').text
        xmin=member.find('bndbox/xmin').text
        ymin = member.find('bndbox/ymin').text
        xmax = member.find('bndbox/xmax').text
        ymax = member.find('bndbox/ymax').text
        # if name=='vertical post completed':
        #     name='vertical_post_completed'
        # elif name=='planned pile':
        #     name='planned_pile'
        # elif name=='pile hole':
        #     name='pile_hole'
        # elif name=='pole rod':
        #     name='pole_rod'
        # elif name=='vertical post augur':
        #     name='vertical_post_augur'
        print(name,xmin,ymin,xmax,ymax)
        values.append(name)
        values.append(xmin)
        values.append(ymin)
        values.append(xmax)
        values.append(ymax)
        values.append(0)
        csvwriter.writerow(values)

def join_csv(filepath,destination):
    files=[]
    csv_merge = open(destination+'combined_csv.csv', 'w')
    for file in os.listdir(filepath):
        print(file)
        files.append(filepath+file)
    for file in files:
        print(file)
        csv_in = open(file)
        for line in csv_in:
            csv_merge.write(line)
        csv_in.close()
    csv_merge.close()
        # files.append(filepath+file)
        # combined_csv = pd.concat([pd.read_csv(f) for f in files])
        # combined_csv.to_csv('/home/naveen/Documents/tpl/tpl-full-data/csvcomb/'+'combined_csv.csv', index=False)


# convert(file)
filepath='/Users/rohanbanerjee/Documents/Takvaviya/Dataset/labelImg/'
print(os.listdir(filepath+'xml/'))
for file in os.listdir(filepath+'xml/'):
    if not file=='.DS_Store':
        convert(filepath, file)
        print(file)
#     print(file)

join_csv('/Users/rohanbanerjee/Documents/Takvaviya/Dataset/labelImg/csvconverted/', '/Users/rohanbanerjee/Documents/Takvaviya/Dataset/labelImg/combined/')


    # resident = []
    # address_list = []
#   if count == 0:
#       name = member.find('Name').tag
#       resident_head.append(name)
#       PhoneNumber = member.find('PhoneNumber').tag
#       resident_head.append(PhoneNumber)
#       EmailAddress = member.find('EmailAddress').tag
#       resident_head.append(EmailAddress)
#       Address = member[3].tag
#       resident_head.append(Address)
#       csvwriter.writerow(resident_head)
#       count = count + 1
#
#   name = member.find('Name').text
#   resident.append(name)
#   PhoneNumber = member.find('PhoneNumber').text
#   resident.append(PhoneNumber)
#   EmailAddress = member.find('EmailAddress').text
#   resident.append(EmailAddress)
#   Address = member[3][0].text
#   address_list.append(Address)
#   City = member[3][1].text
#   address_list.append(City)
#   StateCode = member[3][2].text
#   address_list.append(StateCode)
#   PostalCode = member[3][3].text
#   address_list.append(PostalCode)
#   resident.append(address_list)
#   csvwriter.writerow(resident)
# Resident_data.close()
## Dataset

In the **dataset** folder
For using the dataset, head over to the **new_data** where all the annotated files in the for of .xml files are stored. 
Inside **KittiFormat** the same files in the form of .txt are stored.
All the images could be found inside the **images** folder.

Image size: 698 by 698


---

## Usage of Faster RCNN

Refer to this link: https://github.com/jinfagang/keras_frcnn

---

## Usage of keras_ssd

This folder contains the currently(as of 26/12/2018) used ssd multibox model.

Python version used is 3.6

For using this, first type the following command in your CLI:
**pip install -r requirements_takvaviya.txt**

The dataset used could be found inside the **dataset** folder.

The following is the way the different codes inside are used for:

1. The **ssd7_training.py** is used to train the model. While using this code, the path name of the directories containing the images and dataset has to be changed.
2. model.h5, model01.h5, model02.h5 are the models which are saved right now. The models could be saved after the training as per own requirements.
   Nomenclature of the the model names: e{no. of epochs}s{no. of steps}b{batch size}.h5, for eg: e40s30b60.h5
3. For using the training model in jupyter notebook, use **train.ipynb**.
4. After training the model, the saved model can be used for inference using the **inference.ipynb** code.


